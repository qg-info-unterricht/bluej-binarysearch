
/**
 * Beschreiben Sie hier die Klasse zahlenraten.
 * 
 * @author Frank Schiebel
 * @version 1.0
 */
public class Zahlenraten
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private int [] zahlenreihe;

    /**
     * Konstruktor für Objekte der Klasse zahlenraten
     */
    public Zahlenraten(int anzahl)
    {
        // Zahlenreihe initialisieren
        initZahlenreihe(anzahl);
    }
    
    /** Aufgabe "Binärsuche"
     * 
     * Füge hier den Code einer Methode ein, die nach einer gegebenen
     * Zahl sucht. Als Rückgabe soll der Index des Arrayelements erfolgen,
     * in dem die Zahl gespeichert ist. Wird die Zahl nicht gefunden soll NULL 
     * zurückgegeben werden.
     * 
     * Zähle mit, wieviele Halbierungsschritte nötig sind, um das Ergebnis 
     * zu erhalten.
     */
    
    /** Aufgabe "printer"
     * 
     *  Füge hier eine Methode ein, die die erzeugte Liste wie unten dargestellt
     *  auf der Konsole ausgibt, so dass man den Index und den Wert jedes Arrayelements
     *  sehen kann:
     *  
     *  zahlenreihe[0]=3
     *  zahlenreihe[1]=15
     *  zahlenreihe[2]=18
     *  ...
     */
    public void printZahlenreihe() {
    }

    /**
     * Initialisiere eine geordnete Zahlenreihe 
     * gegebener Laenge
     * 
     * @param  laenge  Die Laenge der Zahlenreihe
     */
    private void initZahlenreihe(int laenge)
    {
        zahlenreihe = new int[laenge];
        int indexvorher = 0;
        for (int i = 0; i < zahlenreihe.length; i++)
        {
            if ( i>0 ) {
                indexvorher = i -1;
            }
            zahlenreihe[i] = zahlenreihe[indexvorher] + (int)(10*Math.random()+1);
        }        
    }
}
